﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluacionFinal
{
    public partial class frmRegistoEmpleados : Form
    {
        public frmRegistoEmpleados()
        {
            InitializeComponent();
        }
        //declaracion de lista que contendra a nuestros empleados
        List<clsEmpleados> empleados = new List<clsEmpleados>();
        int i = 0;//variable contadora para controlar que se ingresen 10 empleados

        private void frmRegistoEmpleados_Load(object sender, EventArgs e)
        {            
            deshabilitarCampos();
        }
                
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            habilitarCampos();
            btnNuevo.Enabled = false;
        }       

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if (i < 10)
            {
                try
                {
                    if (nudISSS.Value == 3 && nudRenta.Value == 10)
                    {
                        clsEmpleados empleado = new clsEmpleados(txtNombres.Text
                                                            , txtApellidos.Text
                                                            , dtpFechaNac.Value
                                                            , txtDui.Text
                                                            , txtCorreo.Text
                                                            , txtCelular.Text
                                                            , dtpFechaContrato.Value
                                                            , txtSueldoBase.Text);
                        empleados.Add(empleado);

                        i++;

                        limpiarCampos();

                        if (i == 10)
                        {
                            MessageBox.Show("Se llenaron los 10 empleados", "Empleados llenos");
                            btnAbrirPlanilla.Enabled = true;
                            btnNuevo.Enabled = false;
                            btnIngresar.Enabled = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Se debe colocar 3% en ISSS y 10% en Renta", "Porcentaje incorrecto");
                    }
                    
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message, "Error");
                    return;
                }
            }                        
            
        }

        private void btnAbrirPlanilla_Click(object sender, EventArgs e)
        {
            dgvEmpleados.Enabled = true;
            btnGenerarPlanilla.Enabled = true;
            btnAbrirPlanilla.Enabled = false;
        }

        private void btnGenerarPlanilla_Click(object sender, EventArgs e)
        {
            dgvEmpleados.DataSource = null;
            dgvEmpleados.DataSource = empleados;
        }

        //procedimiento para deshabilitar los campos
        void deshabilitarCampos()
        {
            nudISSS.Enabled = false;
            nudRenta.Enabled = false;
            btnIngresar.Enabled = false;
            btnGenerarPlanilla.Enabled = false;
            btnAbrirPlanilla.Enabled = false;
            dgvEmpleados.Enabled = false;
            txtNombres.Enabled = false;
            txtApellidos.Enabled = false;
            dtpFechaNac.Enabled = false;
            txtCelular.Enabled = false;
            txtCorreo.Enabled = false;
            txtDui.Enabled = false;
            dtpFechaContrato.Enabled = false;
            txtSueldoBase.Enabled = false;
        }
        //procedimiento para habilitar los campos
        void habilitarCampos()
        {
            nudISSS.Enabled = true;
            nudRenta.Enabled = true;
            btnIngresar.Enabled = true;
            txtNombres.Enabled = true;
            txtApellidos.Enabled = true;
            dtpFechaNac.Enabled = true;
            txtCelular.Enabled = true;
            txtCorreo.Enabled = true;
            txtDui.Enabled = true;
            dtpFechaContrato.Enabled = true;
            txtSueldoBase.Enabled = true;
        }

        //procedmiento para limpiar campos
        void limpiarCampos()
        {
            nudISSS.Value = 0;
            nudRenta.Value = 0;            
            txtNombres.Clear();
            txtApellidos.Clear();            
            txtCelular.Clear();
            txtCorreo.Clear();
            txtDui.Clear();            
            txtSueldoBase.Clear();
        }
    }
}
