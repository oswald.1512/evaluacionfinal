﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace EvaluacionFinal
{
    class clsEmpleados
    {
        //Atributos
        private string nombre;
        private string apellido;
        private DateTime fechaNacimiento;
        private string dui;
        private string correo;
        private string celular;
        private DateTime fechaContrato;
        private double sueldoBase;

        private double sueldoNeto;

        //constructor con parametros
        public clsEmpleados(string nombre, string apellido, DateTime fechaNacimiento, string dui, string correo, string celular, DateTime fechaContrato, string sueldoBase)
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.FechaNacimiento = fechaNacimiento;
            this.Dui = dui;
            this.Correo= correo;
            this.Celular= celular;
            this.fechaContrato= fechaContrato;
            this.SueldoBase= sueldoBase;
            this.SueldoNeto = this.sueldoBase;
        }

        //Propiedades
        public double SueldoNeto
        {
            set
            {
                this.sueldoNeto = calculoSueldoNeto(value);//Caluclo de sueldo neto
            }
            get
            {
                return this.sueldoNeto;
            }
        }        
        public string Nombre
        {
            set
            {
                if (this.esPalabra(value))//Validar el nombre
                {
                    this.nombre = value;
                }
                else
                {
                    throw new Exception("Se debe llenar correctamente el campo Nombre");//Mensaje de error
                }
            }
            get
            {
                return this.nombre;
            }
        }

        public string Apellido
        {
            set
            {
                if (this.esPalabra(value))//Validar el apellido
                {
                    this.apellido = value;
                }
                else
                {
                    throw new Exception("Se debe llenar correctamente el campo Apellido");//Mensaje de error
                }
            }
            get
            {
                return this.apellido;
            }
        }

       

        public string Dui
        {
            set
            {
                if (this.esDui(value))//Validar dui
                {
                    this.dui = value;
                }
                else
                {
                    throw new Exception("Se debe llenar correctamente el campo Dui");//Mensaje de error
                }
            }
            get
            {
                return this.dui;
            }
        }

        public string Correo
        {
            set
            {
                if (this.validarCorreo(value))//Validar correo
                {
                    this.correo = value;
                }
                else
                {
                    throw new Exception("Se debe llenar correctamente el campo para Correo");//Mensaje de error
                }
            }
            get
            {
                return this.correo;
            }
        }
        public string Celular
        {
            set
            {
                if (this.esCelular(value))//Validar celular
                {
                    this.celular = value;
                }
                else
                {
                    throw new Exception("Se debe llenar correctamente el campo Celular");//Mensaje de error
                }
            }
            get
            {
                return this.celular;
            }
        }

        public DateTime FechaNacimiento
        {
            set
            {
                if (this.validarFechaNacieminto(value))//Validar fecha nacimiento
                {
                    this.fechaNacimiento = value;
                }
                else
                {
                    throw new Exception("Se debe llenar correctamente el campo Fecha nacimiento");//Mensaje de error
                }
            }
            get
            {
                return this.fechaNacimiento;
            }
        }

        public DateTime FechaContrato
        {
            set
            {
                if (this.validarFechaContrato(value))//Validar la fecha contrato
                {
                    this.fechaContrato = value;
                }
                else
                {
                    throw new Exception("Se debe llenar correctamente el campo Fecha contrato");//Mensaje de error
                }
            }
            get
            {
                return this.fechaContrato;
            }
        }

        public string SueldoBase
        {
            set
            {
                if (this.esNumero(value))//Validar sueldo base
                {
                    this.sueldoBase = Convert.ToDouble(value);
                }
                else
                {
                    throw new Exception("Se debe llenar correctamente el campo Sueldo base");//Mensaje de error
                }
            }
            get
            {
                return Convert.ToString(this.sueldoBase);
            }
        }

        //METODOS
        //Método de validación sí es letra
        public bool esPalabra(string palabra)
        {
            //cadena de expresiones regulares donde solo se aceptan letras
            Regex regexLetra = new Regex(@"^[a-zA-Z]+$");
            //Valida si no esta vacío          
            if (!String.IsNullOrEmpty(palabra))
            {
                //Valida si es un palabra el dato
                if (regexLetra.IsMatch(palabra))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //Método de validación sí es número
        public bool esNumero(string numero)
        {
            
            //Valida si no esta vacío          
            if (!String.IsNullOrEmpty(numero))
            {
                //Valida si es un número el dato
                if (double.TryParse(numero, out double x))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //Método de validación sí es fecha y esta en el rango
        public bool validarFechaNacieminto(DateTime value)
        {
            //variable que verifica si algo ha sido validado
            bool validado = false;

            //verficamos fecha
            DateTime fechaNacimiento = value;

            //verificamos la fecha del sistema
            int anio = System.DateTime.Now.Year - fechaNacimiento.Year;
            int mes = System.DateTime.Now.Month - fechaNacimiento.Month;
            int dia = System.DateTime.Now.Day - fechaNacimiento.Day;

            //verificamos si su fecha de nacimiento
            if (anio == 0 && mes <= 0 && dia <= 0)
            {
                validado = false;

                
            }
            else
            {
                //si esta bien calculamos la edad
                if (anio>=18 && anio<=50)
                {
                    validado = true;
                }
                    
            }

            return validado;
        }
        //Método de validación sí es contrato
        public bool validarFechaContrato(DateTime value)
        {
            //variable que verifica si algo ha sido validado
            bool validado = false;

           
            DateTime fechaNacimiento = value;

            //verificamos la fecha del sistema
            int anio = System.DateTime.Now.Year - fechaNacimiento.Year;
            int mes = System.DateTime.Now.Month - fechaNacimiento.Month;
            int dia = System.DateTime.Now.Day - fechaNacimiento.Day;

            /*verificamos si su fecha de nacimiento es hoy ó en el futuro*/
            if (anio == 0 && mes <= 0 && dia <= 0)
            {
                validado = false;


            }
            else
            {     
                    validado = true;
            }

            return validado;
        }

        //Método de validación sí es correo
        public bool validarCorreo(string correo)
        {
            string expresion = @"^[\w!#$%&'+-/=?^_`{|}~]+(.[\w!#$%&'+-/=?^_`{|}~]+)*"
                                   + "@"
                                   + @"((([-\w]+.)+[a-zA-Z]{2,4})|(([0-9]{1,3}.){3}[0-9]{1,3}))\z";
            //Valida si no esta vacío          
            if (!String.IsNullOrEmpty(correo))
            {

                if (Regex.IsMatch(correo, expresion))
                {
                    if (Regex.Replace(correo, expresion, string.Empty).Length == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        //Método de validación sí es dui
        public bool esDui(string dui)
        {
            //cadena de expresiones regulares donde solo se aceptan dui
            Regex regexLetra = new Regex(@"^[0-9]{9}");
            //Valida si no esta vacío          
            if (!String.IsNullOrEmpty(dui))
            {
                //Valida si es un palabra el dato
                if (regexLetra.IsMatch(dui))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        //Método de validación sí es celular
        public bool esCelular(string celular)
        {
            //cadena de expresiones regulares donde solo se aceptan celulares
            Regex regexLetra = new Regex(@"^[0-9]{8}");
            //Valida si no esta vacío          
            if (!String.IsNullOrEmpty(celular))
            {
                //Valida si es un palabra el dato
                if (regexLetra.IsMatch(celular))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //Metodo para calcular el sueldo neto
        public double calculoSueldoNeto(double sueldoBase)
        {
            double sueldoNeto = 0;
            double descuentoAfp = sueldoBase * 0.0725;
            double descuentoIss = sueldoBase * 0.03;

            sueldoNeto = sueldoBase - (descuentoAfp + descuentoIss);

            double descuentoRenta = sueldoNeto * 0.1;

            sueldoNeto = sueldoNeto - descuentoRenta;

            return sueldoNeto;
        }

    }
}
